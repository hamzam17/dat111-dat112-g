#ifndef TASKS_H
#define TASKS_H

void task_one(void* queue);
void task_two(void* queue);

#endif