/*
    Hand in: 5
    Description: Functions to handle the process.
*/  

#include <string.h>
#include <Arduino.h>
#include <LiquidCrystal.h>

#include "functions.hpp"
#include "main.hpp"

// Read the pot meters value
int getPot() 
{
    return analogRead(PIN_POT);
}

// Sets the desired led
void setLed(int led, char* toggle) 
{
    int on = 0;
    if (strcmp(toggle, "on") == 0) {
        on = 1;
    }

    byte selected = PORTB_LED_1;
    if (led == 2){
        selected = PORTB_LED_2;
    }

    if (on) {
        PORTB |= selected;
    } else {
        PORTB &= ~selected;
    }
}

// Sets the desired piezo frequency
void setPiezo(int frequency) 
{
    if (frequency == 0) {
        noTone(Pin_buzzer);
    } else {
        tone(Pin_buzzer, frequency);
    }
}

// Sets the LCD text
void setLCD (char* text) 
{   
    LiquidCrystal lcd = getLCD();
    
    // Clear first, then continue.
    lcd.clear();

    if (strlen(text) > 0) {
        lcd.print(text);
    } else {
        lcd.print("");
    }
}