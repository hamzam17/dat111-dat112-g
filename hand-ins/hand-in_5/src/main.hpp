#ifndef MAIN_H
#define MAIN_H

const int Pin_buzzer = 10;
const int PIN_POT = 0;

const byte PORTB_LED_1 = (1 << 0);
const byte PORTB_LED_2 = (1 << 1);
const byte PORTB_PIEZO = (1 << 2);

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;

LiquidCrystal getLCD();

#endif