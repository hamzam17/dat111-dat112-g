/*
    Hand in: 5
    Description: Setup, loop, etc..

    Note:
    1. Instead of two leds, we have a led that shows different colours.
        Led 1 = Red
        Led 2 = Blue    
*/

#include <Arduino.h>
#include <LiquidCrystal.h>
#include "Arduino_FreeRTOS.h"
#include "queue.h"

#include "command.hpp"
#include "tasks.hpp"
#include "main.hpp"

QueueHandle_t queue;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() 
{
    // Serial setup
    Serial.begin(9600);
    Serial.setTimeout(8000);

    // LCD setup
    lcd.begin(16, 2);

    // LED setup
    DDRB |= PORTB_LED_1; // Led 1: (Red) : Digital pin 8.
    DDRB |= PORTB_LED_2; //  Led 2: (Blue) : Digital pin 9. 

    // Piezo setup 
    DDRB |= PORTB_PIEZO; // Digital pin 10.

    // Queues setup
    queue = xQueueCreate(5, sizeof(command_t));

    xTaskCreate(task_one, "Task one", 256, queue, 2, NULL);
    xTaskCreate(task_two, "Task two", 256, queue, 2, NULL);
}

void loop()
{  

}

LiquidCrystal getLCD()
{
    return lcd;
}
