#ifndef command_H
#define command_H

typedef struct
{
    int set;
    char name[50];
    char* arguments[3];
} command_t;

void command_handler(int set, char* command_name, char* argument_1, char* argument_2);

#endif