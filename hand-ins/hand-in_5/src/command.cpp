/*
    Hand in: 5
    Description: Command handler.
*/

#include <string.h>
#include <Arduino.h>

#include "command.hpp"
#include "functions.hpp"

void command_handler(int set, char* command_name, char* argument_1, char* argument_2)
{   
    // If it is a 'set' command.

    if (set) {
        // Convert string to integer.
        int int_argument_1 = atoi(argument_1);

        if (strcmp(command_name, "led") == 0) {
            setLed(int_argument_1, argument_2);
        } else if (strcmp(command_name, "piezo") == 0) {
            setPiezo(int_argument_1);
        } else if (strcmp(command_name, "lcd") ==  0) {
            setLCD(argument_1);
        }
    } else {
        // If it is a 'get' command.
        
        if (strcmp(command_name, "pot") == 0) {
            int value = getPot();
            
            char text[15] = {0};
            sprintf(text, "Value: %i", value);
            Serial.println(text);
        }
    }
}