/*
    Hand in: 5
    Description: Tasks one and towo.
*/

#include <string.h>

#include <Arduino.h>
#include "Arduino_FreeRTOS.h"

#include "queue.h"
#include "tasks.hpp"
#include "command.hpp"

/*
    Task one: Reads and interprets the string and sends it over to task two using queue.
*/

void task_one(void* queue)
{
    command_t command;

    char command_string[50] = {0};
    char *command_arguments[4] = {NULL}; 

    while(1)
    {   
         if (Serial.available()) {
           // Reads from serial input until new line is given/user presses enter.
           int byte = Serial.readBytesUntil('\n', command_string, 50);

           // Terminate the string due to the new line '\n'.
           command_string[byte-1] = '\0';

           // Print the string to the serial
           Serial.println(command_string);
        
           // Split the string so it easier to use afterwards..
           char *splitted_string = strtok(command_string, " ");
           int i = 0;

            // Loop through all the strings that are splitted by a space (" ").
            while (splitted_string != NULL) 
            {
                command_arguments[i] = splitted_string;
                splitted_string = strtok (NULL, " ");
                i++;
            };
            
            char *name = command_arguments[1];
            int set = strcmp(command_arguments[0], "set") == 0;
            int get = strcmp(command_arguments[0], "get") == 0;

            // Quick syntax check of the recieved command: "set ..."/ "get ..."
            if ((get || set) && (strlen(name) > 0)) {

                // Set values for the structure.
                command.set = set;
                strcpy(command.name, name);
                command.arguments[0] = command_arguments[2];
                command.arguments[1] = command_arguments[3];

                // Send it towards the uart.
                xQueueSend(queue, &command, portMAX_DELAY); 
                vTaskDelay(pdMS_TO_TICKS(3000));
            }
        }
    }
}

/*
    Task two: Recieves the commands from task one and performs the action required.
*/

void task_two(void* queue)
{
    while (1)
    {
        command_t command;

        xQueueReceive(queue, &command, portMAX_DELAY);

        taskENTER_CRITICAL(); 

        // Recieved the structure:
        if (command.name != NULL && strlen(command.name) > 0) {
            command_handler(command.set, command.name, command.arguments[0], command.arguments[1]);
        }

        taskEXIT_CRITICAL();
        
        vTaskDelay(pdMS_TO_TICKS(3000));
    }
}

