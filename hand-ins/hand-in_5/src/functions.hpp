#ifndef functions_H
#define functions_H

int getPot();
void setLed(int led, char* toggle);
void setPiezo(int frequency);
void setLCD (char* text);

#endif